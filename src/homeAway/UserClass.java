/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package homeAway;

import data.*;

class UserClass implements MUser {

    private static final long serialVersionUID = 0L;
    private final String idUser;
    private String email;
    private String phone;
    private final String name;
    private final String nationality;
    private String address;
    private boolean owner;
    private final OrderedDictionary<String, CProperty> properties;
    private final IterableStack<CProperty> stay;

    public UserClass(String idUser, String email, String phone, String name, String nationality, String address) {
        this.idUser = idUser;
        this.email = email;
        this.phone = phone;
        this.name = name;
        this.nationality = nationality;
        this.address = address;
        owner = false;
        properties = new OrderedDoublyLL<>();
        stay = new StackInList<CProperty>();
    }

    public void changeUser(String email, String phone, String address) {
        this.email = email;
        this.phone = phone;
        this.address = address;

    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNationality() {
        return nationality;
    }

    public String getaddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getIdUser() {
        return idUser;
    }

    public void insertProperty(MProperty property) {
        properties.insert(property.getidHome().toLowerCase(), property);
        owner = true;
    }

    public void removeProperty(String idHome) {
        properties.remove(idHome);
        if (properties.isEmpty())
            owner = false;
    }

    public boolean getOwner() {
        return owner;
    }

    public Iterator<Entry<String, CProperty>> getProperties() throws NoOwnerPropertysException {
        if (properties.isEmpty())
            throw new NoOwnerPropertysException();
        return properties.iterator();
    }

    public void insertStay(MProperty property) {
        stay.push(property);
    }

    public Iterator<CProperty> staysIterator() throws UserDidntTravelException {
        if (stay.isEmpty())
            throw new UserDidntTravelException();
        return stay.getIterator();
    }

}
