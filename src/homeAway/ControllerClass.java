/*
 * @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package homeAway;

import data.*;

public class ControllerClass implements Controller {

    private static final long serialVersionUID = 0L;
    private static final int DEFAULT_CAPACITY = 10000;
    private final Dictionary<String, MUser> users;
    private final Dictionary<String, MProperty> properties;
    private final Dictionary<String, OrderedDictionary<Ranking, OrderedDictionary<String, CProperty>>> propertiesByRankingAndSpot;
    private final Dictionary<String, OrderedDictionary<Integer, OrderedDictionary<String, CProperty>>> propertiesByPeopleAndSpot;

    public ControllerClass() {
        properties = new ChainedHashTable<>(DEFAULT_CAPACITY);
        users = new ChainedHashTable<>(DEFAULT_CAPACITY);
        propertiesByRankingAndSpot = new ChainedHashTable<>(DEFAULT_CAPACITY);
        propertiesByPeopleAndSpot = new ChainedHashTable<>(DEFAULT_CAPACITY);
    }

    public MUser getUser(String idUser) throws NonExistentUserException {
        if (users.isEmpty())
            throw new NonExistentUserException();
        MUser u = users.find(idUser);
        if (u == null)
            throw new NonExistentUserException();
        return u;
    }

    public MProperty getProperty(String idHome) throws NonExistentPropertyException {
        if (properties.isEmpty())
            throw new NonExistentPropertyException();
        MProperty p = properties.find(idHome.toLowerCase());
        if (p == null)
            throw new NonExistentPropertyException();
        return p;
    }

    public void createsUser(String idUser, String email, String phone, String name, String nationality, String adress)
            throws UserAlreadyExistsException {
        if (!users.isEmpty())
            if (users.find(idUser) != null)
                throw new UserAlreadyExistsException();
        MUser u = new UserClass(idUser, email, phone, name, nationality, adress);
        users.insert(idUser, u);

    }

    public void changeUser(String idUser, String email, String phone, String adress) throws NonExistentUserException {
        getUser(idUser).changeUser(email, phone, adress);
    }

    public void removeUser(String idUser) throws NonExistentUserException, UserIsOwnerException {
        MUser u = getUser(idUser);
        if (u.getOwner())
            throw new UserIsOwnerException();
        users.remove(idUser);
    }

    public void insertProperty(String idHome, String idUser, int price, int people, String spot,
                               String description, String adress)
            throws PriceOrPeopleException, NonExistentUserException, PropertyAlreadyExistsException {

        if (price <= 0 || people <= 0 || people > 20)
            throw new PriceOrPeopleException();
        MUser U = getUser(idUser);
        if (!properties.isEmpty())
            if (properties.find(idHome.toLowerCase()) != null)
                throw new PropertyAlreadyExistsException();
        MProperty P = new PropertyClass(idHome, U, price, people, spot, description, adress);
        properties.insert(idHome.toLowerCase(), P);
        insertPropertyByRankingAndSpot(P);
        insertPropertyByPeopleAndSpot(P);
        U.insertProperty(P);
    }
    
    //Inserts the given property into the rankingAndSpot chainHashTable
    //
    //if theres not a bst inside that CHT creates one and adds it, else just adds the property 
    //into another bst inside this one
    //if theres not a bst inside that bst, creates one and adds it, else just adds the property
    private void insertPropertyByRankingAndSpot(MProperty P) {
    	Ranking ranking = new RankingImpl(P.getScore());
    	OrderedDictionary <Ranking, OrderedDictionary<String, CProperty>> propertiesByRankingAndSpotAVLT
    	= propertiesByRankingAndSpot.find(P.getSpot().toLowerCase());
    	if(propertiesByRankingAndSpotAVLT == null) {
    		propertiesByRankingAndSpotAVLT = new BinarySearchTree<Ranking, OrderedDictionary<String, CProperty>>();
    		propertiesByRankingAndSpot.insert(P.getSpot().toLowerCase(), propertiesByRankingAndSpotAVLT);
    	}
    	OrderedDictionary<String, CProperty> propertiesByRankingAVLT
    	= propertiesByRankingAndSpotAVLT.find(ranking);
    	if(propertiesByRankingAVLT == null) {
    		propertiesByRankingAVLT = new BinarySearchTree<String, CProperty>();
    		propertiesByRankingAndSpotAVLT.insert(ranking, propertiesByRankingAVLT);
    	}
    	propertiesByRankingAVLT.insert(P.getidHome().toLowerCase(), P);
    }
    
  //Inserts the given property into the peopleAndSpot chainHashTable
    //
    //if theres not a bst inside that CHT creates one and adds it, else just adds the property 
    //into another bst inside this one
    //if theres not a bst inside that bst, creates one and adds it, else just adds the property
    private void insertPropertyByPeopleAndSpot(MProperty P) {
    	OrderedDictionary <Integer, OrderedDictionary<String, CProperty>> propertiesByPeopleAndSpotAVLT
    	= propertiesByPeopleAndSpot.find(P.getSpot().toLowerCase());
    	if(propertiesByPeopleAndSpotAVLT == null) {
    		propertiesByPeopleAndSpotAVLT = new BinarySearchTree<Integer, OrderedDictionary<String, CProperty>>();
    		propertiesByPeopleAndSpot.insert(P.getSpot().toLowerCase(), propertiesByPeopleAndSpotAVLT);
    	}
    	OrderedDictionary<String, CProperty> propertiesByPeopleAVLT
    	= propertiesByPeopleAndSpotAVLT.find(P.getPeople());
    	if(propertiesByPeopleAVLT == null) {
    		propertiesByPeopleAVLT = new BinarySearchTree<String, CProperty>();
    		propertiesByPeopleAndSpotAVLT.insert(P.getPeople(), propertiesByPeopleAVLT);
    	}
    	propertiesByPeopleAVLT.insert(P.getidHome().toLowerCase(), P);
    }

    public void removeProperty(String idHome)
            throws NonExistentPropertyException, VisitedPropertyExcpetion, NonExistentUserException {
        MProperty p = getProperty(idHome);
        MUser u = getUser(p.getidUser());
        if (p.getVisited())
            throw new VisitedPropertyExcpetion();
        properties.remove(idHome.toLowerCase());
        removePropertyByRankingAndSpot(p);
        removePropertyByPeopleAndSpot(p);
        u.removeProperty(idHome);
    }
    
    //Removes a property from the Ranking And Spot CHT
    // if the sub trees are empty deletes them
    private void removePropertyByRankingAndSpot(MProperty P) {
        Ranking ranking = new RankingImpl(P.getScore());
    	OrderedDictionary<Ranking, OrderedDictionary<String, CProperty>> propertiesByRankingAndSpotAVLT
        = propertiesByRankingAndSpot.find(P.getSpot().toLowerCase());
        OrderedDictionary<String, CProperty> propertiesByRankingAVLT
                = propertiesByRankingAndSpotAVLT.find(ranking);
        propertiesByRankingAVLT.remove(P.getidHome().toLowerCase());
        if (propertiesByRankingAVLT.isEmpty())
            propertiesByRankingAndSpotAVLT.remove(ranking);
        if (propertiesByRankingAndSpotAVLT.isEmpty())
            propertiesByRankingAndSpot.remove(P.getSpot().toLowerCase());
    }
    
    //Removes a property from the People And Spot CHT
    // if the sub trees are empty deletes them
    private void removePropertyByPeopleAndSpot(MProperty P) {
    	OrderedDictionary<Integer, OrderedDictionary<String, CProperty>> propertiesByPeopleAndSpotAVLT
        = propertiesByPeopleAndSpot.find(P.getSpot().toLowerCase());
        OrderedDictionary<String, CProperty> propertiesByPeopleAVLT
                = propertiesByPeopleAndSpotAVLT.find(P.getPeople());
        propertiesByPeopleAVLT.remove(P.getidHome().toLowerCase());
        if (propertiesByPeopleAVLT.isEmpty())
            propertiesByPeopleAndSpotAVLT.remove(P.getPeople());
        if (propertiesByPeopleAndSpotAVLT.isEmpty())
            propertiesByPeopleAndSpot.remove(P.getSpot().toLowerCase());
    }

    public void Stay(String idUser, String idHome, int score, boolean isOwner) throws NonExistentPropertyException,
            NonExistentUserException, TravelerIsOwnerException, PriceOrPeopleException, NotTheOwnerException {

        if (!isOwner && score <= 0 || score > 20) {
            throw new PriceOrPeopleException();
        }

        MUser U = getUser(idUser);
        MProperty P = getProperty(idHome);

        if (!isOwner) {
            if (P.getidUser().equalsIgnoreCase(idUser))
                throw new TravelerIsOwnerException();
            removePropertyByRankingAndSpot(P);
            P.addScore(score);
            insertPropertyByRankingAndSpot(P);
        } else {
            if (!P.getOwner().equals(U.getName()))
                throw new NotTheOwnerException();
        }
        U.insertStay(P);
        P.setVisited();
    }

    public Iterator<Entry<String, CProperty>> getOwnerProperty(String idUser) throws NonExistentUserException, NoOwnerPropertysException {
        return getUser(idUser).getProperties();
    }

    public Iterator<Entry<Ranking, OrderedDictionary<String, CProperty>>> searchPropertyByRankingAndSpot(String spot)
            throws NoResulstsSearchException {
        if (properties.isEmpty() || properties == null)
            throw new NoResulstsSearchException();
        OrderedDictionary<Ranking, OrderedDictionary<String, CProperty>> d 
        = propertiesByRankingAndSpot.find(spot.toLowerCase());
        if (d == null || d.isEmpty())
            throw new NoResulstsSearchException();
        return d.iterator();
    }
    
    public Iterator<Entry<Integer, OrderedDictionary<String, CProperty>>> searchPropertybyPeopleandSpot(int people, String spot) throws NoResulstsSearchException, PriceOrPeopleException{
        if (people <= 0 || people > 20)
            throw new PriceOrPeopleException();
        if (properties.isEmpty() || properties == null)
            throw new NoResulstsSearchException();
        OrderedDictionary<Integer, OrderedDictionary<String, CProperty>> d 
        = propertiesByPeopleAndSpot.find(spot.toLowerCase());
        if (d == null || d.isEmpty() || d.maxEntry().getKey() < people)
            throw new NoResulstsSearchException();
        return d.iterator();
    }



    public Iterator<CProperty> stayIterator(CUser u) throws UserDidntTravelException {
        return u.staysIterator();

    }


}
