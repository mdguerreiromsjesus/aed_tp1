/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

import java.io.Serializable;

interface MProperty extends Serializable, CProperty {

	/**
	 * Adds a Rating to the Property
	 * 
	 * @param i
	 */

	void addScore(int i);
	
	/**
	 * Sets the property as visited
	 */
	void setVisited();
}
