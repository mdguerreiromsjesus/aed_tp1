/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

public class UserDidntTravelException extends Exception {

	private static final long serialVersionUID = 0L;

	public UserDidntTravelException() {
		super();
	}

	public UserDidntTravelException(String message) {
		super(message);
	}

}
