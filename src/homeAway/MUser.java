/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

import java.io.Serializable;

interface MUser extends Serializable, CUser {

	/**
	 * Changes details about the User
	 * 
	 * @param email
	 * @param phone
	 * @param address
	 */
	void changeUser(String email, String phone, String address);

	

	/**
	 * Inserts a property as owned by the User
	 * 
	 * @param property
	 */
	void insertProperty(MProperty property);

	/**
	 * Removes a property owned by an user
	 * 
	 * @param idHome
	 */

	void removeProperty(String idHome);


	/**
	 * Inserts a stay of the user in a Property property
	 * 
	 * @param property
	 */
	void insertStay(MProperty property);

}
