/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package homeAway;

import java.io.Serializable;

public interface Ranking extends Serializable, Comparable<Ranking> {

	/**
	 * Returns the saved number 
	 * @return ranking
	 */
    Integer getRanking();
    
    /**
     * Compares this number to another
     */
    int compareTo(Ranking o);

}
