/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package homeAway;

public class RankingImpl implements Ranking {

	private static final long serialVersionUID = 0L;
	private final Integer ranking;


    public RankingImpl(Integer ranking) {
        this.ranking = ranking;
    }

    @Override
    public Integer getRanking() {

        return ranking;
    }


    @Override
    public int compareTo(Ranking o) {
        return -ranking.compareTo(o.getRanking());
    }
}
