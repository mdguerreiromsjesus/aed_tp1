/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

class PropertyClass implements MProperty {

	private static final long serialVersionUID = 0L;
	private MUser owner;
	private String idHome;
	private int price;
	private int people;
	private String description;
	private String address;
	private String spot;
	private int score;
	private boolean visited;

	public PropertyClass(String idHome, MUser u, int price, int people, String spot,
			String description, String address) {
		this.owner = u;
		this.idHome = idHome;
		this.price = price;
		this.spot = spot;
		this.people = people;
		this.description = description;
		this.address = address;
		score = 0;
		visited = false;
	}

	public String getidHome() {
		return idHome;
	}

	public String getidUser() {
		return owner.getIdUser();
	}
	
	public int getPrice() {
		return price;
	}

	public int getPeople() {
		return people;
	}

	public String getDescription() {
		return description;
	}

	public String getAddress() {
		return address;
	}

	public String getSpot() {
		return spot;
	}

	public String getOwner() {
		return owner.getName();
	}

	public int getScore() {
		return score;
	}

	public boolean getVisited() {
		return visited;
	}

	public void addScore(int i) {
		score += i;
	}
	
	public void setVisited() {
		visited = true;
	}
}
