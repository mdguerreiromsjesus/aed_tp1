package homeAway;
/**
 * @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
public class TravelerIsOwnerException extends Exception {

	static final long serialVersionUID = 0L;

	public TravelerIsOwnerException() {
		super();
	}

	public TravelerIsOwnerException(String message) {
		super(message);
	}

}
