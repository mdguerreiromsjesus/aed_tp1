/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

import java.io.Serializable;

import data.Entry;
import data.Iterator;
import data.OrderedDictionary;

interface Controller extends Serializable {

  
    /**
     * Adds an User to the System
     *
     * @param idUser
     * @param email
     * @param phone
     * @param name
     * @param nationality
     * @param adress
     * @throws UserAlreadyExistsException
     */
    void createsUser(String idUser, String email, String phone, String name, String nationality, String adress)
            throws UserAlreadyExistsException;

    /**
     * Changes the details about a specified User
     *
     * @param idUser
     * @param email
     * @param phone
     * @param adress
     * @throws NonExistentUserException
     */
    void changeUser(String idUser, String email, String phone, String adress) throws NonExistentUserException;

    /**
     * Removes an User from the System
     *
     * @param idUser
     * @throws NonExistentUserException
     * @throws UserIsOwnerException
     */
    void removeUser(String idUser) throws NonExistentUserException, UserIsOwnerException;

    /**
     * Adds a Property to the System
     *
     * @param idHome
     * @param idUser
     * @param price
     * @param people
     * @param spot
     * @param description
     * @param adress
     * @throws PriceOrPeopleException
     * @throws NonExistentUserException
     * @throws PropertyAlreadyExistsException
     */
    void insertProperty(String idHome, String idUser, int price, int people, String spot, String description,
                        String adress) throws PriceOrPeopleException, NonExistentUserException, PropertyAlreadyExistsException;

    /**
     * Removes a Property from the System
     *
     * @param idHome
     * @throws NonExistentPropertyException
     * @throws VisitedPropertyExcpetion
     * @throws NonExistentUserException 
     */
    void removeProperty(String idHome) throws NonExistentPropertyException, VisitedPropertyExcpetion, NonExistentUserException;


    /**
     * Adds a Stay when is visited either by a Traveller or an Owner
     *
     * @param idUser
     * @param idHome
     * @param score
     * @param isOwner
     * @throws NonExistentUserException
     * @throws NonExistentPropertyException
     * @throws NotTheOwnerException
     * @throws TravelerIsOwnerException
     * @throws PriceOrPeopleException
     */
    void Stay(String idUser, String idHome, int score, boolean isOwner) throws NonExistentUserException, NonExistentPropertyException, NotTheOwnerException, TravelerIsOwnerException, PriceOrPeopleException;
    
    /**
	 * Returns a User based on his ID
	 * 
	 * @param idUser
	 * @return User
	 * @throws NonExistentUserException
	 */
	MUser getUser(String idUser) throws NonExistentUserException;

	/**
	 * Returns a Property based on it's idHome
	 * 
	 * @param idHome
	 * @return Property
	 * @throws NonExistentPropertyException
	 */
	MProperty getProperty(String idHome) throws NonExistentPropertyException;
	
	/**
	 * Returns an iterator of the properties owned by an user
	 * @param idUser
	 * @return iterator of the properties
	 * @throws NonExistentUserException
	 * @throws NoOwnerPropertysException
	 */
	Iterator<Entry<String, CProperty>> getOwnerProperty(String idUser) throws NonExistentUserException, NoOwnerPropertysException;

	/**
	 * Returns an iterator with Ranking values of the Properties with the given spot
	 * @param spot
	 * @return Iterator
	 * @throws NoResulstsSearchException
	 * @throws PriceOrPeopleException
	 */
    Iterator<Entry<Ranking, OrderedDictionary<String, CProperty>>> searchPropertyByRankingAndSpot(String spot)
            throws NoResulstsSearchException;
	
	/**
	 * Returns an iterator with People values of the Properties with the given spot
	 * @param people
	 * @param spot
	 * @return Iterator
	 * @throws NoResulstsSearchException
	 * @throws PriceOrPeopleException
	 */
    Iterator<Entry<Integer, OrderedDictionary<String, CProperty>>> searchPropertybyPeopleandSpot(int people, String spot)
			throws NoResulstsSearchException, PriceOrPeopleException;
	 
	/**
	 * Returns an iterator with the stays of a User u
	 * 
	 * @param u
	 * @return Iterator
	 * @throws UserDidntTravelException
	 */
	Iterator<CProperty> stayIterator(CUser u) throws UserDidntTravelException;

}
