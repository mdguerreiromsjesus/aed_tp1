/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

public class InvalidPositionException extends RuntimeException {

	static final long serialVersionUID = 0L;

	public InvalidPositionException() {
		super();
	}

	public InvalidPositionException(String message) {
		super(message);
	}

}
