/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package homeAway;

public class PropertyAlreadyExistsException extends Exception {

	static final long serialVersionUID = 0L;

	public PropertyAlreadyExistsException() {
		super();
	}

	public PropertyAlreadyExistsException(String message) {
		super(message);
	}

}
