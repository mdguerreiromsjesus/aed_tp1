/*
 * @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package homeAway;

import java.io.Serializable;


public interface CProperty extends Serializable {

    /**
     * Returns lower cased property ID
     *
     * @return String idHome
     */
    String getidHome();

    /**
     * Returns an User ID
     *
     * @return String idUser
     */
    String getidUser();


    /**
     * Returns the price of a rental
     *
     * @return int price
     */
    int getPrice();

    /**
     * Returns the maximum capacity of each Property
     *
     * @return int capacity
     */
    int getPeople();

    /**
     * Returns a description of a Property
     *
     * @return String description
     */
    String getDescription();

    /**
     * Returns the address of a Property
     *
     * @return String address
     */

    String getAddress();

    /**
     * Returns the location of the Property
     *
     * @return String spot
     */

    String getSpot();

    /**
     * Returns the name of the owner of the Property
     *
     * @return String name
     */

    String getOwner();

    /**
     * Returns the rating of the Property
     *
     * @return int score
     */
    int getScore();

    /**
     * Returns <code>true</code> if the Property is visited.
     *
     * @return boolean isVisited
     */

    boolean getVisited();

}
