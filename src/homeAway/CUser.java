/*
 * @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package homeAway;

import java.io.Serializable;

import data.Entry;
import data.Iterator;

public interface CUser extends Serializable {

    /**
     * Returns the Username
     *
     * @return String username
     */
    String getName();

    /**
     * Returns Users Email
     *
     * @return String email
     */

    String getEmail();

    /**
     * Returns Users Nationality
     *
     * @return String nationality
     */
    String getNationality();

    /**
     * Returns Users Address
     *
     * @return String address
     */

    String getaddress();

    /**
     * Returns users phone number
     *
     * @return String phonenumber
     */

    String getPhone();

    /**
     * Returns the Users ID
     *
     * @return String idUser
     */

    String getIdUser();

    /**
     * Checks whether the User is the owner of a Property
     *
     * @return<code>true</code> if the user is an owner
     */
    boolean getOwner();

    /**
     * Returns an Iterator of the properties owned by this user
     *
     * @return Property
     */
    Iterator<Entry<String, CProperty>> getProperties() throws NoOwnerPropertysException;

    /**
     * Returns an iterator of stays of the user
     *
     * @return Iterator
     * @throws UserDidntTravelException
     */
    Iterator<CProperty> staysIterator() throws UserDidntTravelException;

}
