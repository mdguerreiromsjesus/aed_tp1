package data;

/**
 * Ordered Dictionary interface
 *
 * @param <K> Generic type Key, must extend comparable
 * @param <V> Generic type Value
 * @author AED team
 * @version 1.0
 */
public interface OrderedDictionary<K extends Comparable<K>, V>
        extends Dictionary<K, V> {

    /**
     * Returns the entry with the smallest key in the dictionary.
     *
     * @return entry
     * @throws EmptyDictionaryException
     */
    Entry<K, V> minEntry() throws EmptyDictionaryException;

    /**
     * Returns the entry with the largest key in the dictionary.
     *
     * @return entry
     * @throws EmptyDictionaryException
     */
    Entry<K, V> maxEntry() throws EmptyDictionaryException;

    /**
     * Returns an iterator of the entries in the dictionary
     * which preserves the key order relation.
     *
     * @return Iterator
     */
    Iterator<Entry<K,V>> iterator( );

}

