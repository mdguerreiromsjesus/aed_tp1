/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package data;

public interface IterableStack<E> extends Stack<E>{
	
	/**
     * Returns an iterator of the stack
     * @return iterator
     */
    Iterator<E> getIterator();

}
