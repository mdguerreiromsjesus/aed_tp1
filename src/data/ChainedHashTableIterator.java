
package data;

class ChainedHashTableIterator<K, V> implements Iterator<Entry<K, V>> {


    private static final long serialVersionUID = 0L;


    private final Dictionary<K, V>[] table;

    private int current;

    private Iterator<Entry<K, V>> entryIt;


    public ChainedHashTableIterator(Dictionary<K, V>[] table) {
        this.table = table;
        this.rewind();
    }

   
    @Override
    public boolean hasNext() {
        return entryIt != null && this.entryIt.hasNext();
    }

    @Override
    public Entry<K, V> next() {
        if (!this.hasNext())
            throw new NoSuchElementException();
        Entry<K, V> next = entryIt.next();
        if (!entryIt.hasNext())
            entryIt = this.dictionaryIterator(++current);
        return next;
    }

    @Override
    public void rewind() {
        entryIt = this.dictionaryIterator(0);
    }

    
    Iterator<Entry<K, V>> dictionaryIterator(int position) {
        Dictionary<K, V> dictionary;
        for (int i = position; i < table.length; i++) {
            dictionary = table[i];
            if (!dictionary.isEmpty()) {
                current = i;
                return dictionary.iterator();
            }
        }
        return null;
    }

}
