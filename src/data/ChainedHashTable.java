
package data;


public class ChainedHashTable<K extends Comparable<K>, V> extends
        HashTable<K, V> {

    static final long serialVersionUID = 0L;

    protected Dictionary<K, V>[] table;


    public ChainedHashTable(int capacity) {
        initialize(capacity);
    }

    public ChainedHashTable() {
        this(DEFAULT_CAPACITY);
    }


    protected int hash(K key) {
        return Math.abs(key.hashCode()) % table.length;
    }


    public V find(K key) {
        return table[hash(key)].find(key);
    }


    public V insert(K key, V value) {
        if (this.isFull())
            this.rehash();
        V entryValue = table[hash(key)].insert(key, value);
        if (entryValue == null)
            currentSize++;
        return entryValue;
    }


    public V remove(K key) {
        if (this.isEmpty())
            return null;
        V entryValue = table[hash(key)].remove(key);
        if (entryValue != null)
            currentSize--;
        return entryValue;
    }


    public Iterator<Entry<K, V>> iterator() {
        return new ChainedHashTableIterator<>(this.table);
    }


    protected void rehash() {
        Iterator<Entry<K, V>> it = this.iterator();
        initialize(maxSize * 2);
        while (it.hasNext()) {
            Entry<K, V> entry = it.next();
            this.insert(entry.getKey(), entry.getValue());
        }
    }

    //Creates an hashTable with the given size
    @SuppressWarnings("unchecked")
    private void initialize(int capacity) {
        int arraySize = HashTable.nextPrime((int) (1.1 * capacity));
        table = (Dictionary<K, V>[]) new Dictionary[arraySize];
        for (int i = 0; i < arraySize; i++)
           table[i] = new OrderedDoublyLL<>();
        maxSize = capacity;
        currentSize = 0;
    }

}
