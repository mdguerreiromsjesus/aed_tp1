/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt  
 */
package data;

class EntryClass<K, V> implements Entry<K, V> {

    private static final long serialVersionUID = 0L;
    private K k;
    private V v;

    EntryClass(K k, V v) {
        this.k = k;
        this.v = v;
    }

    @Override
    public K getKey() {
        return k;
    }

    @Override
    public V getValue() {
        return v;
    }
    
    @Override
    public void setKey(K key) {
    	k = key;
    }
    
    @Override
    public void setValue(V value) {
    	v = value;
    }

}
