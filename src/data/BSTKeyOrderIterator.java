
package data;

class BSTKeyOrderIterator<K extends Comparable<K>, V> implements
        Iterator<Entry<K, V>> {

  
    private static final long serialVersionUID = 0L;
    protected BSTNode<K, V> root;
    protected Stack<BSTNode<K, V>> path;

 
    public BSTKeyOrderIterator(BSTNode<K, V> root) {
        this.root = root;
        rewind();
    }

    @Override
    public boolean hasNext() {
        return (!path.isEmpty());
    }

    @Override
    public Entry<K, V> next() throws NoSuchElementException {
        if (!hasNext())
            throw new NoSuchElementException();
        BSTNode<K, V> node = path.pop();
        Entry<K, V> entryToReturn = node.getEntry();
        if (node.getRight() != null)
            minNode(node.getRight());
        return entryToReturn;
    }

    @Override
    public void rewind() {
        path = new StackInList<BSTNode<K, V>>();
        minNode(root);
    }


    private void minNode(BSTNode<K, V> node) {
        while (node != null) {
            path.push(node);
            node = node.getLeft();
        }
    }
}
