/*
  @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */
package data;


public class OrderedDoublyLL<K extends Comparable<K>, V> implements OrderedDictionary<K, V> {

    /**
     * Serial Version UID of the Class
     */
    private static final long serialVersionUID = 0L;

    /**
     *  Node at the head of the list.
     */
    private DListNode<Entry<K, V>> head;

    /**
     * Node at the tail of the list.
     */
    private DListNode<Entry<K, V>> tail;

    /**
     * Number of elements in the list.
     */
    private int currentSize;


    /**
     * Constructor of an empty ordered doubly linked list.
     * head and tail are initialized as null.
     * currentSize is initialized as 0.
     */
    public OrderedDoublyLL() {
        head = null;
        tail = null;
        currentSize = 0;
    }

    @Override
    public boolean isEmpty() {
        return currentSize == 0;
    }

    @Override
    public int size() {
        return currentSize;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new DoublyLLIterator<>(head, tail);
    }

    @Override
    public V find(K key) {
        V entryValue = null;
        if (!this.isEmpty()) {
            DListNode<Entry<K, V>> node = findNode(key);
            if (node != null && node.getElement().getKey().compareTo(key) == 0)
                entryValue = node.getElement().getValue();
        }
        return entryValue;
    }

    @Override
    public V insert(K key, V value) {
        Entry<K, V> newEntry = new EntryClass<>(key, value);
        if (this.isEmpty()) {
            addFirst(newEntry);
            return null;
        }
        DListNode<Entry<K, V>> node = findNode(key);
        if (node != null) {
            Entry<K, V> entry = node.getElement();
            if (entry.getKey().compareTo(key) == 0) {
                V oldEntryValue = entry.getValue();
                node.setElement(newEntry);
                return oldEntryValue;
            } else if (entry.getKey().compareTo(key) > 0) {
                if (node == head)
                    addFirst(newEntry);
                else
                    addMiddle(newEntry, node);
                return null;
            }
        }
        addLast(newEntry);
        return null;
    }

    @Override
    public V remove(K key) {
        if (this.isEmpty())
            return null;
        DListNode<Entry<K, V>> node = findNode(key);
        if (node != null) {
            Entry<K, V> entry = node.getElement();
            if (entry.getKey().compareTo(key) == 0) {
                V entryValue = entry.getValue();
                if (node == head)
                    removeFirstNode();
                else if (node == tail)
                    removeLastNode();
                else
                    removeMiddleNode(node);
                return entryValue;
            }
        }
        return null;
    }


    @Override
    public Entry<K, V> minEntry() throws EmptyDictionaryException {
        if (this.isEmpty())
            throw new EmptyDictionaryException();
        return head.getElement();
    }

    @Override
    public Entry<K, V> maxEntry() throws EmptyDictionaryException {
        if (this.isEmpty())
            throw new EmptyDictionaryException();
        return tail.getElement();
    }


    protected DListNode<Entry<K, V>> findNode(K key) {
        DListNode<Entry<K, V>> node = head;
        while (node != null && node.getElement().getKey().compareTo(key) < 0)
            node = node.getNext();
        return node;
    }


    protected void addFirst(Entry<K, V> element) {
        DListNode<Entry<K, V>> newNode = new DListNode<>(element, null, head);
        if (this.isEmpty())
            tail = newNode;
        else
            head.setPrevious(newNode);
        head = newNode;
        currentSize++;
    }


    protected void addLast(Entry<K, V> element) {
        DListNode<Entry<K, V>> newNode = new DListNode<>(element, tail, null);
        if (this.isEmpty())
            head = newNode;
        else
            tail.setNext(newNode);
        tail = newNode;
        currentSize++;
    }


    protected void addMiddle(Entry<K, V> element, DListNode<Entry<K, V>> node) {
        DListNode<Entry<K, V>> PrevNode = node.getPrevious();
        DListNode<Entry<K, V>> newNode = new DListNode<>(element, PrevNode, node);
        PrevNode.setNext(newNode);
        node.setPrevious(newNode);
        currentSize++;
    }


    protected void removeFirstNode() {
        head = head.getNext();
        if (head == null)
            tail = null;
        else
            head.setPrevious(null);
        currentSize--;
    }


    protected void removeLastNode() {
        tail = tail.getPrevious();
        if (tail == null)
            head = null;
        else
            tail.setNext(null);
        currentSize--;
    }


    protected void removeMiddleNode(DListNode<Entry<K, V>> node) {
        DListNode<Entry<K, V>> prevNode = node.getPrevious();
        DListNode<Entry<K, V>> nextNode = node.getNext();
        prevNode.setNext(nextNode);
        nextNode.setPrevious(prevNode);
        currentSize--;
    }

}
