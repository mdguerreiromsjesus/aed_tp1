/*
 * @author Miguel Santana Jesus (49875) ms.jesus@campus.fct.unl.pt
 * @author Miguel de Oliveira Guerreiro (49876) md.guerreiro@campus.fct.unl.pt
 */

import data.Entry;
import data.Iterator;
import data.OrderedDictionary;
import homeAway.*;

import java.io.*;
import java.util.Scanner;

class Main {
    private static final String DATA_FILE = "ctr.dat";
    private static final String EXIT = "XS";
    private static final String EXITING = "Gravando e terminando...\n";
    private static final String ADD_USER = "IU";
    private static final String CHANGE_USER = "UU";
    private static final String REMOVE_USER = "RU";
    private static final String CONSULTE_USER = "GU";
    private static final String ADD_PROPERTY = "AH";
    private static final String REMOVE_PROPERTY = "RH";
    private static final String CONSULTE_PROPERTY = "GH";
    private static final String STAY = "AT";
    private static final String LIST_PROPERTYS = "LH";
    private static final String LIST_STAYS = "LT";
    private static final String SEARCH_PROPERTYS = "PH";
    private static final String LIST_BETTER_PROP = "LB";
    private static final String FAILED_SEARCH = "Pesquisa nao devolveu resultados.\n";
    private static final String USER_DIDNT_TRAVEL = "Utilizador nao viajou.\n";
    private static final String USER_NOT_OWNER = "Utilizador nao e proprietario.\n";
    private static final String PROPERTY_LIST = "%s %s %s %s %d %d %d\n";
    private static final String STAY_ADDED = "Estadia adicionada com sucesso.\n";
    private static final String TRAVELER_NOT_OWNER = "Viajante nao e o proprietario.\n";
    private static final String PROPERTY_DATA = "%s: %s, %s, %d, %d, %d, %s\n";
    private static final String NO_PROPERTY = "Propriedade inexistente.\n";
    private static final String VISITED_PROPERTY = "Propriedade ja foi visitada.\n";
    private static final String PROPERTY_REMOVED = "Propriedade removida com sucesso.\n";
    private static final String INVALID_DATA = "Dados invalidos.\n";
    private static final String PROPERTY_EXISTS = "Propriedade existente.\n";
    private static final String PROPERTY_ADDED = "Propriedade adicionada com sucesso.\n";
    private static final String USER_DATA = "%s: %s, %s, %s, %s\n";
    private static final String USER_REMOVED = "Utilizador removido com sucesso.\n";
    private static final String USER_IS_OWNER = "Utilizador e proprietario.\n";
    private static final String USER_CHANGED = "Utilizador atualizado com sucesso.\n";
    private static final String NO_USER = "Utilizador inexistente.\n";
    private static final String USER_INSERTED = "Insercao de utilizador com sucesso.\n";
    private static final String USER_EXISTS = "Utilizador existente.\n";
    private static final String TRAVELER_IS_OWNER = "Viajante e o proprietario.\n";

    public static void main(String[] args) {
        Main.commands();
    }

    private static void commands() {
        ControllerClass ctr = load();
        Scanner input = new Scanner(System.in);
        String command = input.next().toUpperCase();
        while (!command.equals(EXIT)) {
            switch (command) {
                case ADD_USER:
                    addUser(input, ctr);
                    break;
                case CHANGE_USER:
                    changeUser(input, ctr);
                    break;
                case REMOVE_USER:
                    removeUser(input, ctr);
                    break;
                case CONSULTE_USER:
                    consulteUser(input, ctr);
                    break;
                case ADD_PROPERTY:
                    addProperty(input, ctr);
                    break;
                case REMOVE_PROPERTY:
                    removeProperty(input, ctr);
                    break;
                case CONSULTE_PROPERTY:
                    consulteProperty(input, ctr);
                    break;
                case STAY:
                    stay(input, ctr);
                    break;
                case LIST_PROPERTYS:
                    listProperties(input, ctr);
                    break;
                case LIST_STAYS:
                    listStays(input, ctr);
                    break;
                case SEARCH_PROPERTYS:
                    searchProperties(input, ctr);
                    break;
                case LIST_BETTER_PROP:
                    listBetterProps(input, ctr);
                    break;
                default:
                    break;
            }
            command = input.next().toUpperCase();
        }
        System.out.println(EXITING);
        save(ctr);
    }

    private static void save(ControllerClass ctr) {
        try {
            ObjectOutputStream file = new ObjectOutputStream(new FileOutputStream(DATA_FILE));
            file.writeObject(ctr);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ControllerClass load() {
        try {
            ObjectInputStream file = new ObjectInputStream(new FileInputStream(DATA_FILE));
            ControllerClass ctr = (ControllerClass) file.readObject();
            file.close();
            return ctr;
        } catch (IOException | ClassNotFoundException e) {
            return new ControllerClass();
        }
    }

    private static void listBetterProps(Scanner input, ControllerClass ctr) {
        try {
            String spot = input.next() + input.nextLine();
            Iterator<Entry<Ranking, OrderedDictionary<String, CProperty>>> iterator = ctr.searchPropertyByRankingAndSpot(spot);
            while (iterator.hasNext()) {
                Iterator<Entry<String, CProperty>> iterator2 = iterator.next().getValue().iterator();
                while (iterator2.hasNext())
                    propertyInfo(iterator2.next().getValue());
            }
            System.out.println();
        } catch (NoResulstsSearchException e) {
            System.out.println(FAILED_SEARCH);
        }

    }
    
    private static void propertyInfo(CProperty p) {
        System.out.printf(PROPERTY_LIST, p.getidHome(), p.getDescription(), p.getAddress(), p.getSpot(), p.getPrice(),
                p.getPeople(), p.getScore());
    }

    private static void searchProperties(Scanner input, ControllerClass ctr) {
        try {
            int people = input.nextInt();
            String spot = input.next() + input.nextLine();
            Iterator<Entry<Integer, OrderedDictionary<String, CProperty>>> iterator = ctr.searchPropertybyPeopleandSpot(people, spot);
            while (iterator.hasNext()) {
                Entry<Integer, OrderedDictionary<String, CProperty>> next = iterator.next();
                if (next.getKey() >= people) {
                    Iterator<Entry<String, CProperty>> iterator2 = next.getValue().iterator();
                    while (iterator2.hasNext())
                        propertyInfo(iterator2.next().getValue());
                }
            }
            System.out.println();
        } catch (NoResulstsSearchException e) {
            System.out.println(FAILED_SEARCH);
        } catch (PriceOrPeopleException e) {
            System.out.println(INVALID_DATA);
        }


    }


    private static void listStays(Scanner input, ControllerClass ctr) {
        try {
            String idUser = input.next().toLowerCase();
            CUser u = ctr.getUser(idUser);
            Iterator<CProperty> stays = ctr.stayIterator(u);
            while (stays.hasNext()) {
                CProperty p = stays.next();
                propertyInfo(p);
            }
            System.out.println();
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        } catch (UserDidntTravelException e) {
            System.out.println(USER_DIDNT_TRAVEL);
        }

    }

    private static void listProperties(Scanner input, ControllerClass ctr) {
        try {
            String idUser = input.next().toLowerCase();
            Iterator<Entry<String, CProperty>> iterator = ctr.getOwnerProperty(idUser);
            while (iterator.hasNext())
                propertyInfo(iterator.next().getValue());
            System.out.println();
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        } catch (NoOwnerPropertysException e) {
            System.out.println(USER_NOT_OWNER);
        }

    }

    private static void stay(Scanner input, ControllerClass ctr) {
        String idUser = input.next().toLowerCase();
        String idHome = input.next();
        int score = 0;
        try {
            if (!input.hasNextInt())
                ctr.Stay(idUser, idHome, score, true);
            else {
                score = input.nextInt();
                ctr.Stay(idUser, idHome, score, false);
            }
            input.nextLine();
            System.out.println(STAY_ADDED);
        } catch (NonExistentPropertyException e) {
            System.out.println(NO_PROPERTY);
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        } catch (TravelerIsOwnerException e) {
            System.out.println(TRAVELER_IS_OWNER);
        } catch (PriceOrPeopleException e) {
            System.out.println(INVALID_DATA);
        } catch (NotTheOwnerException e) {
            System.out.println(TRAVELER_NOT_OWNER);
        }
    }

    private static void consulteProperty(Scanner input, ControllerClass ctr) {
        try {
            String idHome = input.next();
            CProperty p = ctr.getProperty(idHome);
            System.out.printf(PROPERTY_DATA, p.getDescription(), p.getAddress(), p.getSpot(), p.getPrice(),
                    p.getPeople(), p.getScore(), p.getOwner());
            System.out.println();
        } catch (NonExistentPropertyException e) {
            System.out.println(NO_PROPERTY);
        }

    }

    private static void removeProperty(Scanner input, ControllerClass ctr) {
        try {
            String idHome = input.next().toLowerCase();
            ctr.removeProperty(idHome);
            System.out.println(PROPERTY_REMOVED);
        } catch (NonExistentPropertyException e) {
            System.out.println(NO_PROPERTY);
        } catch (VisitedPropertyExcpetion e) {
            System.out.println(VISITED_PROPERTY);
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        }

    }

    private static void addProperty(Scanner input, ControllerClass ctr) {
        try {
            String idHome = input.next();
            String idUser = input.next().toLowerCase();
            int price = input.nextInt();
            int people = input.nextInt();
            String spot = input.next() + input.nextLine();
            String description = input.nextLine();
            String adress = input.nextLine();
            ctr.insertProperty(idHome, idUser, price, people, spot, description, adress);
            System.out.println(PROPERTY_ADDED);
        } catch (PriceOrPeopleException e) {
            System.out.println(INVALID_DATA);
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        } catch (PropertyAlreadyExistsException e) {
            System.out.println(PROPERTY_EXISTS);
        }

    }

    private static void consulteUser(Scanner input, ControllerClass ctr) {
        try {
            String idUser = input.next().toLowerCase();
            CUser u = ctr.getUser(idUser);
            System.out.printf(USER_DATA, u.getName(), u.getaddress(), u.getNationality(), u.getEmail(), u.getPhone());
            System.out.println();
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        }

    }

    private static void removeUser(Scanner input, ControllerClass ctr) {
        try {
            String idUser = input.next().toLowerCase();
            ctr.removeUser(idUser);
            System.out.println(USER_REMOVED);
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        } catch (UserIsOwnerException e) {
            System.out.println(USER_IS_OWNER);
        }

    }

    private static void changeUser(Scanner input, ControllerClass ctr) {
        try {
            String idUser = input.next().toLowerCase();
            String email = input.next();
            String phone = input.next();
            input.nextLine();
            String adress = input.nextLine();
            ctr.changeUser(idUser, email, phone, adress);
            System.out.println(USER_CHANGED);
        } catch (NonExistentUserException e) {
            System.out.println(NO_USER);
        }

    }

    private static void addUser(Scanner input, ControllerClass ctr) {
        try {
            String idUser = input.next().toLowerCase();
            String email = input.next();
            String phone = input.next();
            String name = input.next() + input.nextLine();
            String nationality = input.nextLine();
            String adress = input.nextLine();
            ctr.createsUser(idUser, email, phone, name, nationality, adress);
            System.out.println(USER_INSERTED);
        } catch (UserAlreadyExistsException e) {
            System.out.println(USER_EXISTS);
        }
    }

}
